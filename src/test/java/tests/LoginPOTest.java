package tests;

import io.github.cdimascio.dotenv.Dotenv;
import io.qameta.allure.Description;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.HomePage;
import utils.DataDriven;
import utils.TestListener;

@Listeners({TestListener.class})
public class LoginPOTest extends TestBase{

    private final String LOGIN_DATA = "src/test/java/data/login.csv";

    @Test
    @Description("Verify that user can login successfully")
    public void loginSuccessfully(){

        String pageName = homePage
                .clickLogin()
                .login("selenium@autotest.com", "12345")
                .getPageName();
        Assert.assertEquals(pageName, "MY ACCOUNT");
    }

    @Test(dataProvider = "invalidLoginCSVData")
    @Description("Verify that user cannot login with invalid email/password")
    public void loginWithInvalidInfoFromCSVFile(String email, String password, String expectedPageName){

        String pageName = homePage
                .clickLogin()
                .loginWithInvalidInfo(email, password)
                .getPageName();
        Assert.assertEquals(expectedPageName, pageName);
    }

    @DataProvider(name = "invalidLoginCSVData")
    private Object[][] getInvalidLoginInfoFromCSVFile(){
        return DataDriven.getCSVData(LOGIN_DATA);
    }
}
