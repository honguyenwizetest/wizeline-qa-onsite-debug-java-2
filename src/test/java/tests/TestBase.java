package tests;

import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.*;
import pages.HomePage;

public class TestBase {

    protected WebDriver driver;
    protected HomePage homePage;

    private String URL;
    private String HEADLESS;

    public WebDriver getDriver(){
        return this.driver;
    }

    @BeforeTest
    public void getConfig(){
        Dotenv dotenv = Dotenv.configure().directory("./").load();
        URL = dotenv.get("URL");
        HEADLESS = dotenv.get("HEADLESS");
        System.out.println("Headless mode: " + HEADLESS);
    }

    @BeforeMethod
    @Parameters("browser")
    public void testSetup(@Optional("chrome") String browser){
        System.out.println("Browser: " + browser);
        if(browser.equalsIgnoreCase("firefox")){
            System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/geckodriver");
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(Boolean.parseBoolean(HEADLESS));
            driver = new FirefoxDriver(options);
        } else {
            System.setProperty("webdriver.chrome.driver", "./src/test/resources/drivers/chromedriver");
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(Boolean.parseBoolean(HEADLESS));
            driver = new ChromeDriver(options);
        }

        driver.get(URL);
        homePage = new HomePage(driver);
    }
}
