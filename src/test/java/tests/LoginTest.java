package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest {

    @Test
    public void testLogin(){
        WebDriver driver;
        driver = new ChromeDriver();
        driver.get("http://automationpractice.com");

        driver.findElement(By.cssSelector("a.login")).click();
        driver.findElement(By.id("email")).sendKeys("selenium@autotest.com");
        driver.findElement(By.id("passwd")).sendKeys("12345");
        driver.findElement(By.id("SubmitLogin")).click();

        Assert.assertEquals("MY ACCOUNT", driver.findElement(By.cssSelector("h1.page-heading")).getText());
        driver.quit();
    }
}
