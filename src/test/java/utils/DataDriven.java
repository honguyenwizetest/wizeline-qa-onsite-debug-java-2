package utils;

import com.opencsv.CSVReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DataDriven {

    public static Object[][] getCSVData(String filePath) {

        Object[][] csvData = null;
        try {
            FileReader fileReader = new FileReader(filePath);
            CSVReader csvReader = new CSVReader(fileReader);

            String[] header = csvReader.readNext();
            String[] nextRecord;
            List dataRows = new ArrayList();

            while ((nextRecord = csvReader.readNext()) != null) {
                dataRows.add(nextRecord);
            }

            csvData = new Object[dataRows.size()][header.length];

            for (int i = 0; i < dataRows.size(); i++) {
                csvData[i] = (Object[])dataRows.get(i+1);
            }

        } catch (Exception ex){
            System.out.println("getCSVData exception: " + ex.getMessage());
        } finally {
            return csvData;
        }
    }
}
