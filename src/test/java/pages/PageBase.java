package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageBase {
    protected WebDriver driver;

    @FindBy(css = "h1.page-heading")
    private WebElement txtPageName;

    public PageBase(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step("Get page name")
    public String getPageName(){
        return txtPageName.toString();
    }
}
