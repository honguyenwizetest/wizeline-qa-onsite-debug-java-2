package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageBase{

    @FindBy(id = "email")
    private WebElement txtEmail;

    @FindBy(id = "passwd")
    private WebElement txtPassword;

    @FindBy(id = "SubmitLogin")
    private WebElement btnLogin;

    public LoginPage(WebDriver driver){
        super(driver);
    }

    private void doLogin(String email, String password){
        txtEmail.sendKeys(email);
        txtPassword.sendKeys(password);
        btnLogin.submit();
    }

    @Step("Enter user name and password and click login")
    public MyAccountPage login(String email, String password){
        doLogin(email, password);
        return new MyAccountPage(driver);
    }

    @Step("Enter user name and password and click login")
    public LoginPage loginWithInvalidInfo(String email, String password){
        doLogin(email, password);
        return this;
    }
}
