package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase{

    @FindBy(xpath = "//*[@id='header']/div[3]/div/div/nav/div[1]/a")
    private WebElement btnSignIn;

    public HomePage(WebDriver driver){
        super(driver);
    }

    @Step("Clicks sign in button at home page")
    public LoginPage clickLogin(){
        btnSignIn.click();
        return new LoginPage(driver);
    }
}
